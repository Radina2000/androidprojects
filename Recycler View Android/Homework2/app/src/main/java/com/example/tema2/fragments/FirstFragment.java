package com.example.tema2.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.tema2.R;
import com.example.tema2.adapters.UserAdapter;
import com.example.tema2.interfaces.Links;
import com.example.tema2.interfaces.OnItemClickListener;
import com.example.tema2.models.Album;
import com.example.tema2.models.Post;
import com.example.tema2.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FirstFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private Links links;
    private ArrayList<User> usersList = new ArrayList<>();
    private UserAdapter userAdapter = new UserAdapter(usersList, new OnItemClickListener() {
        @Override
        public void onUserClick(User user) {
            links.openSecondFragment(user);
        }

        @Override
        public void onAlbumClick(Album album) {
        }

        @Override
        public void onArrowClick(User user) {
            getPostsForUser(user);
        }

    }, new Links() {
        @Override
        public void openSecondFragment(User user) {
            AppCompatActivity appCompatActivity = (AppCompatActivity) getView().getContext();
            SecondFragment secondFragment = new SecondFragment(user);
            appCompatActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frag1, secondFragment).addToBackStack("null").commit();
        }

        @Override
        public void openThirdFragment(Album album) {

        }
    });
    private String mParam1;
    private String mParam2;

    public FirstFragment() {
    }

    public static FirstFragment newInstance(String param1, String param2) {
        FirstFragment fragment = new FirstFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
//        setUpRecyclerView();
        getUsers();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_1, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.users_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(userAdapter);


        return view;
    }

    public void getPostsForUser(User user) {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url = "https://jsonplaceholder.typicode.com/users";
        url += "/" + user.getId() + "/posts";
        StringRequest getPostsRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            handlePostResponse(response, user);
                        } catch (JSONException exception) {
                            exception.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "ERROR", Toast.LENGTH_LONG).show();
                    }
                }
        );
        queue.add(getPostsRequest);
    }

    public void handlePostResponse(String response, User user) throws JSONException {

        user.getPosts().clear();
        JSONArray postsJsonArray = new JSONArray(response);
        for (int index = 0; index < postsJsonArray.length(); index++) {
            JSONObject postObject = postsJsonArray.getJSONObject(index);

            if (postObject != null) {
                int userId = postObject.getInt("userId");
                int id = postObject.getInt("id");
                String title = postObject.getString("title");
                String body = postObject.getString("body");
                Post post = new Post(title, id);
                if (!user.getPosts().contains(post))
                    user.getPosts().add(post);
            }
        }
        userAdapter.notifyDataSetChanged();
    }

    public void getUsers() {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url = "https://jsonplaceholder.typicode.com/users";
        StringRequest getUsersRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            handleUserResponse(response);
                        } catch (JSONException exception) {
                            exception.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "ERROR", Toast.LENGTH_LONG).show();
                    }
                }

        );
        queue.add(getUsersRequest);
    }

    public void handleUserResponse(String response) throws JSONException {

        JSONArray userJsonArray = new JSONArray(response);

        for (int index = 0; index < userJsonArray.length(); index++) {

            JSONObject userObject = userJsonArray.getJSONObject(index);
            String name = userObject.getString("name");
            int id = userObject.getInt("id");
            User user = new User(name, id);

            usersList.add(user);
        }
        userAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof Links) {
            links = (Links) context;
        }
    }
}