package com.example.shopapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.shopapp.R;
import com.example.shopapp.adapters.ShopAppAdapter;
import com.example.shopapp.interfaces.ActivitiesFragmentsCommunication;
import com.example.shopapp.interfaces.OnItemClick;
import com.example.shopapp.models.Category;
import com.example.shopapp.singleton.VolleyConfigSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.shopapp.constants.Constants.BASE_URL;
import static com.example.shopapp.constants.Constants.ID;
import static com.example.shopapp.constants.Constants.TITLE;
import static com.example.shopapp.fragments.HomePage.TAG_HOME_PAGE;
import static com.example.shopapp.fragments.Welcome.TAG_WELCOME;

public class Home extends Fragment {

    public static final String TAG_HOME = "TAG_HOME";

    private OnItemClick onItemClick;
    private ActivitiesFragmentsCommunication fragmentCommunication;

    ArrayList<Category> categories = new ArrayList<>();
    ShopAppAdapter adapter=new ShopAppAdapter(categories,onItemClick);

    public static Home newInstance() {

        Bundle args = new Bundle();

        Home fragment = new Home();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_home, container, false);
    }



    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ActivitiesFragmentsCommunication) {
            fragmentCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpRecycleView(view);
        getCategories();
        onAddHomePageFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    void setUpRecycleView(View view) {


        RecyclerView recyclerView=view.findViewById(R.id.category_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(),RecyclerView.HORIZONTAL,false);
        recyclerView.setLayoutManager(layoutManager);
        //categories.clear();
        adapter = new ShopAppAdapter(categories,onItemClick);
        recyclerView.setAdapter(adapter);

    }

    void getCategories() {
        VolleyConfigSingleton volleyConfigSingleton = VolleyConfigSingleton.getInstance(getContext());
        RequestQueue queue = volleyConfigSingleton.getRequestQueue();

       String url = BASE_URL + "/Radina2000/json_android/categories";
       

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            handlePostsResponse(response);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        queue.add(stringRequest);

    }

    void handlePostsResponse(String response) throws JSONException {

        //categories.clear();

        JSONArray jsonArray = new JSONArray(response);
        for (int index = 0; index < jsonArray.length(); index++) {
            JSONObject categoryJSON = (JSONObject) jsonArray.get(index);
            if (categoryJSON != null) {

                String title = categoryJSON.getString(TITLE);
                String id=categoryJSON.getString(ID);
                Category category = new Category(title);
                categories.add(category);

            }

        }
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }

    }

    private void onAddHomePageFragment() {
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frame_layout_home, HomePage.newInstance(), TAG_HOME_PAGE);

        fragmentTransaction.commit();
    }
}
