package com.example.tema2.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.tema2.R;
import com.example.tema2.adapters.AlbumAdapter;
import com.example.tema2.interfaces.Links;
import com.example.tema2.interfaces.OnItemClickListener;
import com.example.tema2.models.Album;
import com.example.tema2.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class SecondFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private Links links;
    private User user;
    private ArrayList<Album> albumsList = new ArrayList<>();
    private AlbumAdapter albumAdapter = new AlbumAdapter(albumsList, new OnItemClickListener() {
        @Override
        public void onUserClick(User user) {
        }

        @Override
        public void onAlbumClick(Album album) {

            links.openThirdFragment(album);
        }

        @Override
        public void onArrowClick(User user) {

        }
    }, new Links() {
        @Override
        public void openSecondFragment(User user) {

        }

        @Override
        public void openThirdFragment(Album album) {
            AppCompatActivity appCompatActivity = (AppCompatActivity) getView().getContext();
            ThirdFragment thirdFragment = new ThirdFragment(album);
            appCompatActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frag2, thirdFragment).addToBackStack("null").commit();
        }
    });
    private String mParam1;
    private String mParam2;

    public SecondFragment(User user) {
        this.user = user;
    }
    public static SecondFragment newInstance(String param1, String param2, User user) {
        SecondFragment fragment = new SecondFragment(user);
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        getAlbumsForUser(this.user);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_2, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.albums_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(albumAdapter);

        return view;
    }

    public void getAlbumsForUser(User user) {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url = "https://jsonplaceholder.typicode.com/users";
        url += "/" + user.getId() + "/albums";

        StringRequest getAlbumsRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            handleAlbumResponse(response);
                        } catch (JSONException exception) {
                            exception.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "ERROR", Toast.LENGTH_LONG).show();
                    }
                }
        );
        queue.add(getAlbumsRequest);
    }

    public void handleAlbumResponse(String response) throws JSONException {

        albumsList.clear();
        JSONArray albumsJsonArray = new JSONArray(response);
        for (int index = 0; index < albumsJsonArray.length(); index++) {
            JSONObject albumObject = albumsJsonArray.getJSONObject(index);

            if (albumObject != null) {
                int userId = albumObject.getInt("userId");
                int id = albumObject.getInt("id");
                String title = albumObject.getString("title");
                Album album = new Album(id, title);
                if (!albumsList.contains(album))
                    albumsList.add(album);
            }
        }
        albumAdapter.notifyDataSetChanged();
    }
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof Links) {
            links = (Links) context;
        }
    }

}