package com.example.shopapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.shopapp.R;
import com.example.shopapp.interfaces.ActivitiesFragmentsCommunication;

public class Welcome extends Fragment {
    public static final String TAG_WELCOME="TAG_WELCOME";

    private ActivitiesFragmentsCommunication fragmentCommunication;

    public static Welcome newInstance() {

        Bundle args = new Bundle();

        Welcome fragment = new Welcome();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_welcome,container,false);


    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if(context instanceof ActivitiesFragmentsCommunication){
            fragmentCommunication=(ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        view.findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                goToLogin();
            }
        });

        view.findViewById(R.id.btn_register).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                goToRegister();

            }
        });
    }
    private void goToLogin(){

        if(fragmentCommunication!=null)
        {
            fragmentCommunication.onReplaceFragment(Login.TAG_LOGIN);
        }
    }
    private void goToRegister(){

        if(fragmentCommunication!=null)
        {
            fragmentCommunication.onReplaceFragment(Register.TAG_REGISTER);
        }
    }

}
