package com.example.shopapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


import com.example.shopapp.HomeActivity;
import com.example.shopapp.R;
import com.example.shopapp.helpers.UtilsValidators;
import com.example.shopapp.interfaces.ActivitiesFragmentsCommunication;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.BuildConfig;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends Fragment {

    public static final String TAG_LOGIN = "TAG_LOGIN";
    private ActivitiesFragmentsCommunication fragmentCommunication;
    private FirebaseAuth auth;
    private ProgressBar progressBar;

    public static Login newInstance() {

        Bundle args = new Bundle();

        Login fragment = new Login();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        auth = FirebaseAuth.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);


    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication) {
            fragmentCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (BuildConfig.DEBUG) {
            EditText editTextEmail = view.findViewById(R.id.edt_email);
            editTextEmail.setText("beatrice.bartos@yahoo.com");

            EditText editTextPassword = view.findViewById(R.id.edt_password);
            editTextPassword.setText("Test1234");
        }

        progressBar = view.findViewById(R.id.progress_bar);
        view.findViewById(R.id.btn_register).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                goToRegister();

            }
        });
        view.findViewById(R.id.btn_forgotPassword).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                goToForgotPassword();

            }
        });

        view.findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                new TaskInBackground().execute();
                progressBar.setVisibility(View.VISIBLE);
                validateEmailAndPassword();

            }
        });

    }

    private void validateEmailAndPassword() {

        if (getView() == null) {
            return;
        }
        EditText emailEdtText = getView().findViewById(R.id.edt_email);
        EditText passwordEdtText = getView().findViewById(R.id.edt_password);

        String email = emailEdtText.getText().toString().trim();

        String password = passwordEdtText.getText().toString();

        if (!UtilsValidators.isValidEmail(email)) {
            emailEdtText.setError("Invalid Email");
            return;
        } else {
            emailEdtText.setError(null);
        }
        if (!UtilsValidators.isValidPassword(password)) {
            passwordEdtText.setError("Invalid Password");
            return;
        } else {
            passwordEdtText.setError(null);
        }
        loginFirebaseUser(email, password);


    }

    private void loginFirebaseUser(String email, String password) {

        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    //FirebaseUser user=auth.getCurrentUser();
                    progressBar.setVisibility(View.GONE);
                    goToHome();
                    Toast.makeText(getContext(), "Authentication success", Toast.LENGTH_SHORT).show();


                } else {

                    Toast.makeText(getContext(), "Authentication failed", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);

                }
            }
        });

    }

    private void goToHome() {
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        startActivity(intent);

        getActivity().finish();


    }

    private void goToRegister() {

        if (fragmentCommunication != null) {
            fragmentCommunication.onReplaceFragment(Register.TAG_REGISTER);
        }
    }

    private void goToForgotPassword() {

        if (fragmentCommunication != null) {
            fragmentCommunication.onReplaceFragment(ForgotPassword.TAG_FORGOT_PASSWORD);
        }
    }

    public class TaskInBackground extends AsyncTask<Void,Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Handler handler =  new Handler(getContext().getMainLooper());
            handler.post( new Runnable(){
                public void run(){
                    Toast.makeText(getContext(), "Waiting for authentication",Toast.LENGTH_LONG).show();
                }
            });
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

        }
    }
}