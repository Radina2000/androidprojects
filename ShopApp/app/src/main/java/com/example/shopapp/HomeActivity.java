package com.example.shopapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.shopapp.fragments.ForgotPassword;
import com.example.shopapp.fragments.Home;
import com.example.shopapp.fragments.HomePage;
import com.example.shopapp.fragments.Login;
import com.example.shopapp.fragments.Register;
import com.example.shopapp.fragments.Welcome;
import com.example.shopapp.interfaces.ActivitiesFragmentsCommunication;
import com.google.firebase.auth.FirebaseAuth;

import static com.example.shopapp.fragments.ForgotPassword.TAG_FORGOT_PASSWORD;
import static com.example.shopapp.fragments.Home.TAG_HOME;
import static com.example.shopapp.fragments.HomePage.TAG_HOME_PAGE;
import static com.example.shopapp.fragments.Login.TAG_LOGIN;
import static com.example.shopapp.fragments.Register.TAG_REGISTER;
import static com.example.shopapp.fragments.Welcome.TAG_WELCOME;

public class HomeActivity extends AppCompatActivity implements ActivitiesFragmentsCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        onAddHomeFragment();

    }

    private void onAddHomeFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.home_frame_layout, Home.newInstance(), TAG_HOME);

        fragmentTransaction.commit();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.right_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                FirebaseAuth.getInstance().signOut();
                goToWelcome();
                return true;
            case R.id.about:
                Toast.makeText(this, "About Item ", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.page_home:
                onReplaceFragment(TAG_HOME);
                Toast.makeText(this, "Home Page ", Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void goToWelcome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }



    @Override
    public void onReplaceFragment(String TAG) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment;

        switch (TAG) {
            case TAG_HOME: {
                fragment = Home.newInstance();
                break;
            }

            default:
                return;
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.home_frame_layout, fragment, TAG);

        fragmentTransaction.commit();
    }
}