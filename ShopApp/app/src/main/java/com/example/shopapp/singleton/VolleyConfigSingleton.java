package com.example.shopapp.singleton;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import androidx.annotation.Nullable;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

public class VolleyConfigSingleton {


    private static VolleyConfigSingleton singletonInstance;
    private RequestQueue singletonRequestQueue;
    private static Context context;

    private ImageLoader imageLoader;

    private VolleyConfigSingleton(Context context) {
        this.context = context;
        this.singletonRequestQueue = getRequestQueue();

        this.imageLoader = new ImageLoader(singletonRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap>
                    cache = new LruCache<String, Bitmap>(20);


            @Nullable
            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {

                cache.put(url, bitmap);
            }
        });
    }

    public RequestQueue getRequestQueue() {
        if (singletonRequestQueue == null) {
            singletonRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return singletonRequestQueue;
    }

    public ImageLoader getImageLoader(){
        return imageLoader;
    }

    public static synchronized VolleyConfigSingleton getInstance(Context context){
        if(singletonInstance==null)
        {
            singletonInstance=new VolleyConfigSingleton(context);

        }
        return singletonInstance;
    }

    public <T> void addToRequestQueue(Request<T> request){
        getRequestQueue().add(request);

    }
}
