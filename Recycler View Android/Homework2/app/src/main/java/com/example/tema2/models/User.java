package com.example.tema2.models;

import java.util.ArrayList;

public class User {

    private String name;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int id;

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public ArrayList<Post> getPosts() {
        return posts;
    }

    public void setPosts(ArrayList<Post> posts) {
        this.posts = posts;
    }

    private ArrayList<Post> posts;
    private boolean expanded;

    public User(String name,int id){
        this.name=name;
        this.id=id;
        posts=new ArrayList<>();
        this.expanded=false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
