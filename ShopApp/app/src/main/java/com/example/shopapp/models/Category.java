package com.example.shopapp.models;

public class Category {
    private String title;

    public String getTitle() {
        return title;
    }

    public Category(String title) {
        this.title = title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
