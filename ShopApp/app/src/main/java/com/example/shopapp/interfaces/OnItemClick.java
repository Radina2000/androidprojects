package com.example.shopapp.interfaces;

import com.example.shopapp.models.Category;

public interface OnItemClick {
    public void categoryItemClick(Category category);
}
