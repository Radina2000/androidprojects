package com.example.tema2.interfaces;

import com.example.tema2.models.Album;
import com.example.tema2.models.User;

public interface Links {

    public void openSecondFragment(User user);
    public void openThirdFragment(Album album);
}
