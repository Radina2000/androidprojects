package com.example.shopapp.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.shopapp.R;
import com.example.shopapp.helpers.UtilsValidators;
import com.example.shopapp.interfaces.ActivitiesFragmentsCommunication;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;


public class ForgotPassword extends Fragment {

    public static final String TAG_FORGOT_PASSWORD = "TAG_FORGOT_PASSWORD";
    private ActivitiesFragmentsCommunication fragmentCommunication;
    private FirebaseAuth auth;

    public static ForgotPassword newInstance() {

        Bundle args = new Bundle();

        ForgotPassword fragment = new ForgotPassword();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        auth=FirebaseAuth.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forgot_password, container, false);


    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);


        if (context instanceof ActivitiesFragmentsCommunication) {
            fragmentCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.btn_forgotPassword).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                validateEmail();
            }
        });

    }
    private void goToLogin() {

        if (fragmentCommunication != null) {
            fragmentCommunication.onReplaceFragment(Login.TAG_LOGIN);
        }
    }
    private void resetPassword(String email)
    {
        auth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getContext(), "The email was sent successfully!", Toast.LENGTH_SHORT).show();
                            goToLogin();
                        } else {
                            Toast.makeText(getContext(), "The email was not sent successfully!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    private void validateEmail() {

        if (getView() == null) {
            return;
        }
        EditText emailEdtText = getView().findViewById(R.id.edt_email);

        String email = emailEdtText.getText().toString().trim();

        if (!UtilsValidators.isValidEmail(email)) {
            emailEdtText.setError("Invalid Email");
            return;
        } else {
            emailEdtText.setError(null);
        }
        resetPassword(email);
    }
}