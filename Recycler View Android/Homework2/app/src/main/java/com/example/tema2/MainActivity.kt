package com.example.tema2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.tema2.fragments.FirstFragment
import com.example.tema2.fragments.SecondFragment
import com.example.tema2.fragments.ThirdFragment
import com.example.tema2.interfaces.Links
import com.example.tema2.models.Album
import com.example.tema2.models.User

class MainActivity : AppCompatActivity() , Links {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addFragment1()
    }
    fun addFragment1(){
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        val tag = FirstFragment::class.java.name
        val addTransaction = transaction.add(
            R.id.frame_layout, FirstFragment.newInstance("",""), tag
        )
        addTransaction.commit()
    }

    override fun openThirdFragment(album: Album) {
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        val tag = ThirdFragment::class.java.name
        val replaceTransaction = transaction.replace(
            R.id.frame_layout, ThirdFragment.newInstance("","",album), tag
        )
        replaceTransaction.addToBackStack(tag)
        replaceTransaction.commit()
    }

    override fun openSecondFragment(user: User) {
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        val tag = SecondFragment::class.java.name
        val replaceTransaction = transaction.replace(
            R.id.frame_layout, SecondFragment.newInstance("","",user), tag
        )
        replaceTransaction.addToBackStack(tag)
        replaceTransaction.commit()

    }
    }
